<?php
use yii\widgets\Pjax;
use yii\bootstrap\Html;
use dmstr\widgets\Alert;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $shortLinkModel app\models\Shortlink */
/* @var $form ActiveForm */

$this->title = 'Link Shorter';
?>

<div class="box box-primary">
    <div class="box-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="box-body">
        <?php Pjax::begin()?>
            <?php $form = ActiveForm::begin([
                'enableClientValidation' => true,
                'options'=>[
                    'data-pjax'=>true,
                ]
            ]); ?>
        
            <?= Alert::widget()?>
        
            <?= $form->field($shortLinkModel, 'long_url') ?>

            <div class="form-group">
                <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
            </div>
        
            <?php ActiveForm::end(); ?>
        <?php Pjax::end()?>
    </div>
</div>
