<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Shortlink;
use yii\web\NotFoundHttpException;

/**
 * Description of SiteController
 *
 * @author Кирилл
 */
class SiteController extends Controller
{

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Lists all Transaction models.
     * @return mixed
     */
    public function actionIndex()
    {
        $shortLinkModel = new Shortlink();
        if ($shortLinkModel->load(Yii::$app->request->post()) && $shortLinkModel->save()) {
            Yii::$app->session->setFlash('success',
                'Your short URL is: '.$shortLinkModel->shortUrl);
            $shortLinkModel = new Shortlink();
        }
        return $this->render('index',
                [
                'shortLinkModel' => $shortLinkModel
        ]);
    }
    /**
     *
     * @param sting $shortCode
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionRedirect($shortCode)
    {
        //Get id from code
        $recordId = Shortlink::getIDFromShortenedURL($shortCode);
        //Find real Url
        $longUrl  = Shortlink::find()
            ->select(['long_url'])
            ->andWhere(['id' => $recordId])
            ->scalar();
        if (is_null($longUrl)) {
            throw new NotFoundHttpException('Url not found');
        }
        return $this->redirect($longUrl, 301);
    }
}