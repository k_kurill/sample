<?php
namespace app\models;

use yii\helpers\Url;

/**
 * This is the model class for table "shotlink".
 *
 * @property integer $id
 * @property string $long_url original Url to site
 */
class Shortlink extends \yii\db\ActiveRecord
{
    //Alphabet for id encoding
    const ALLOWED_CHARS = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

    public function getShortUrl()
    {
        return Url::to([
            '/site/redirect',
            'shortCode' => self::getShortenedURLFromID($this->id)
            ],true);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shortlink';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['long_url'], 'url'],
            [['long_url'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'long_url' => 'Long Url',
        ];
    }

    /**
     * Converts id using base string
     *
     * @param int $index
     * @param string $base
     * @return string
     */
    public static function getShortenedURLFromID($index, $base = self::ALLOWED_CHARS)
    {
        $length = strlen($base);
        $out    = '';
        while ($index > $length - 1) {
            $position = (int) fmod($index, $length);
            $out      = $base{$position}.$out;
            $index  = (int) floor($index / $length);
        }
        return $base{$index}.$out;
    }

    /**
     * Converts base short code to ID
     * @param string $shortCode
     * @param string $base
     * @return int decoded id;
     */
    public static function getIDFromShortenedURL($shortCode,
                                                 $base = self::ALLOWED_CHARS)
    {
        $length     = strlen($base);
        $size       = strlen($shortCode) - 1;
        $shortCode  = str_split($shortCode);
        $out        = strpos($base, array_pop($shortCode));
        foreach ($shortCode as $i => $char) {
            $out += strpos($base, $char) * pow($length, $size - $i);
        }
        return $out;
    }
}