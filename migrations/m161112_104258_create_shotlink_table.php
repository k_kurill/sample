<?php

use yii\db\Migration;

/**
 * Handles the creation of table `shotlink`.
 */
class m161112_104258_create_shotlink_table extends Migration
{

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('shortlink',
            [
            'id' => $this->primaryKey(),
            'long_url' => $this->text(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('shortlink');
    }
}